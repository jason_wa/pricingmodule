/*
/!**
 * Created by Jason on 8/28/2018.
 *!/
import DiscountChargeCalculator from './src/discountChargeCalculator';

describe('DiscountChargeInfo Test', function() {
    let discountChargeInfo = new DiscountChargeCalculator();
    const itemInfo = {
        isApplicablePromotion: false,
        itemPrice: 1.00,
        quantity: 3,
        taxObj:[
            {
                taxId: "123",
                taxRate: 0.05,
                extraInfo: {
                    isTaxIncrease: true,
                    priceThreshold: 4,
                    taxIncreaseRate: 0.08
                }
            }
        ],
        discountObj:[
            {
                discount: 4.25,
                discountIsPer: false,
                discountRate: 0.00
            }
        ],
        chargeObj:[],
        options:[
            {
                optionPrice: 1.00,
                quantity: 5,
                subOptions:[
                    {
                        optionPrice: 1.00,
                        quantity: 4
                    }
                ]
            }
        ],
        extraInfo:{
            categoryId: 2,
            isQuantityRule: true,
            quantityThreshold: 6
        }
    };
    let itemPriceInfo = discountChargeInfo.itemDiscountChargeCalculator(itemInfo);
    const expectedResult = {
        itemSubtotal: 78,
        itemDiscount: 4.25,
        itemCharge: 0,
        itemPromotion: 0
    };

    it('itemSubtotal', function() {
        assert.equal(itemPriceInfo.itemSubtotal,78);
    });
    it('itemDiscount', function() {
        assert.equal(itemPriceInfo.itemDiscount,4.25);
    });
    it('itemCharge', function() {
        assert.equal(itemPriceInfo.itemCharge,0);
    });
    it('itemPromotion', function() {
        assert.equal(itemPriceInfo.itemPromotion,0);
    });
});*/
