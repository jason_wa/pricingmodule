/**
 * Created by Jason on 8/28/2018.
 */
import CanadaOntarioTaxCalculator from './src/CanadaOntarioTaxCalculator';
import DiscountChargeCalculator from './src/discountChargeCalculator';
import OrderPricing from './src/orderPricing';

describe('DiscountChargeInfo Test', function() {
    let taxCalculator = new CanadaOntarioTaxCalculator();
    let discountChargeCalculator = new DiscountChargeCalculator();
    let orderPricing = new OrderPricing(discountChargeCalculator,taxCalculator);

    const orderInfo = {
        totalTips: 10,
        roundingStrategy: "NEAREST_5_CENTS",
        isTaxWithDiscount: true,
        isTaxWithCharge: false,
        isTaxExempt: false,
        discountObj:[
            {
                discount: 0,
                discountIsPer: true,
                discountRate: 0.04
            }
        ],
        chargeObj:[
            {
                charge: 0,
                chargeIsPer: true,
                chargeRate: 0.04
            }
        ],
        itemInfoList:[
            {
                isApplicablePromotion: true,
                isSkipDiscount: false,
                itemPrice: 1.00,
                quantity: 3,
                taxObj:[
                    {
                        taxId: "123",
                        taxRate: 0.05,
                        extraInfo: {
                            isTaxIncrease: true,
                            priceThreshold: 104,
                            taxIncreaseRate: 0.08
                        }
                    }
                ],
                discountObj:[
                    {
                        discount: 4.25,
                        discountIsPer: false,
                        discountRate: 0.00
                    }
                ],
                chargeObj:[],
                options:[
                    {
                        optionPrice: 1.00,
                        quantity: 5,
                        subOptions:[
                            {
                                optionPrice: 1.00,
                                quantity: 4
                            }
                        ]
                    }
                ],
                extraInfo:{
                    categoryId: 2,
                    isQuantityRule: true,
                    quantityThreshold: 3
                }
            },
            {
                isApplicablePromotion: true,
                isSkipDiscount: false,
                itemPrice: 1.00,
                quantity: 3,
                taxObj:[
                    {
                        taxId: "12345",
                        taxRate: 0.05,
                        extraInfo: {
                            isTaxIncrease: true,
                            priceThreshold: 104,
                            taxIncreaseRate: 0.08
                        }
                    }
                ],
                discountObj:[
                    {
                        discount: 4.25,
                        discountIsPer: false,
                        discountRate: 0.00
                    }
                ],
                chargeObj:[],
                options:[
                    {
                        optionPrice: 1.00,
                        quantity: 5,
                        subOptions:[
                            {
                                optionPrice: 1.00,
                                quantity: 4
                            }
                        ]
                    }
                ],
                extraInfo:{
                    categoryId: 6,
                    isQuantityRule: true,
                    quantityThreshold: 3
                }
            }
        ],
        parentOrder:{
            suborderNum: 4, // to divide shared dish quantity
            itemInfoList:[
                {
                    isApplicablePromotion: false,
                    isSkipDiscount: false,
                    itemPrice: 1.00,
                    quantity: 3,
                    taxObj:[
                        {
                            taxId: "123",
                            taxRate: 0.05,
                            extraInfo:{
                                isTaxIncrease: true,
                                priceThreshold: 4,
                                taxIncreaseRate: 0.08
                            }
                        }
                    ],
                    discountObj:[],
                    chargeObj:[],
                    options:[
                        {
                            optionPrice: 1.00,
                            quantity: 5,
                            subOptions:[
                                {
                                    optionPrice: 1.00,
                                    quantity: 4
                                }
                            ]
                        }
                    ],
                    extraInfo:{
                        categoryId: 3,
                        isQuantityRule: true,
                        quantityThreshold: 3
                    }
                }
            ]
        }
    };

    let orderDetailInfo = orderPricing.orderDetailPrice(orderInfo);
    it('orderSubtotal', function() {
        assert.equal(orderDetailInfo.orderSubtotal,150);
    });
    it('orderDiscount', function() {
        assert.equal(orderDetailInfo.orderDiscount,6);
    });
    it('orderCharge', function() {
        assert.equal(orderDetailInfo.orderCharge,6);
    });
    it('orderPromotion', function() {
        assert.equal(orderDetailInfo.orderPromotion,130.5);
    });
    it('orderTotalTax', function() {
        assert.equal(orderDetailInfo.orderTaxTotal,13.71);
    });
    it('orderTotalTips', function() {
        assert.equal(orderDetailInfo.orderTotalTips,10);
    });
    it('orderOriginalTotal', function() {
        assert.equal(orderDetailInfo.orderOriginalTotal,173.71);
    });
    it('rounding', function() {
        assert.equal(orderDetailInfo.rounding,0.01);
    });
    it('orderTotal', function() {
        assert.equal(orderDetailInfo.orderTotal,173.7);
    });
});
