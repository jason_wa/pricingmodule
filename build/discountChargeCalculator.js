"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var DiscountChargeCalculator = function () {
    function DiscountChargeCalculator() {
        _classCallCheck(this, DiscountChargeCalculator);
    }

    _createClass(DiscountChargeCalculator, [{
        key: "itemDiscountChargeCalculator",
        value: function itemDiscountChargeCalculator(itemInfo) {
            var itemSubtotal = 0;
            var itemCharge = 0;
            var itemDiscount = 0;
            var itemOptionsPrice = 0;
            var isPromotion = itemInfo.isApplicablePromotion;
            var itemPromotion = 0;
            var itemDiscountInfo = itemInfo.discountObj;
            var itemChargeInfo = itemInfo.chargeObj;

            // item subtotal
            for (var i in itemInfo.options) {
                var itemSuboptPrice = 0;
                var optInfo = itemInfo.options[i];
                for (var j in optInfo.subOptions) {
                    var subOptInfo = optInfo.subOptions[j];
                    itemSuboptPrice += subOptInfo.optionPrice * subOptInfo.quantity;
                }
                itemOptionsPrice += (optInfo.optionPrice + itemSuboptPrice) * optInfo.quantity;
            }

            itemSubtotal += (itemInfo.itemPrice + itemOptionsPrice) * itemInfo.quantity;

            // item discount
            for (var i in itemDiscountInfo) {
                var discountInfo = itemDiscountInfo[i];
                if (!!discountInfo.discountIsPer) {
                    itemDiscount += itemSubtotal * discountInfo.discountRate * itemInfo.quantity;
                } else {
                    itemDiscount += discountInfo.discount * itemInfo.quantity;
                }
            }

            // item charge
            for (var i in itemChargeInfo) {
                var chargeInfo = itemChargeInfo[i];
                if (chargeInfo.chargeIsPer) {
                    itemCharge += itemSubtotal * chargeInfo.chargeRate * itemInfo.quantity;
                } else {
                    itemCharge += chargeInfo.charge * itemInfo.quantity;
                }
            }

            // item promotion
            if (!!isPromotion) {
                itemPromotion += itemSubtotal - itemDiscount + itemCharge;
            }

            itemInfo.itemPriceDetail = {
                itemSubtotal: itemSubtotal,
                itemDiscount: itemDiscount,
                itemCharge: itemCharge,
                itemPromotion: itemPromotion
            };
        }
    }, {
        key: "addOrderDiscountInfo2Item",
        value: function addOrderDiscountInfo2Item(orderInfo, itemInfo, orderSubtotal, orderTotalDiscount) {
            var orderDiscountObj = orderInfo.discountObj;
            var priceDetail = itemInfo.itemPriceDetail;
            var itemTotalPrice = priceDetail.itemSubtotal - priceDetail.itemDiscount + priceDetail.itemCharge;
            var orderDiscount = 0;
            for (var i in orderDiscountObj) {
                if (!!orderDiscountObj[i].discountIsPer) {
                    if (!itemInfo.isSkipDiscount) {
                        orderDiscount += itemTotalPrice * orderDiscountObj[i].discountRate;
                    }
                } else {
                    if (orderSubtotal > 0) {
                        orderDiscount += itemTotalPrice / orderSubtotal * orderDiscountObj[i].discount;
                    }
                }
            }

            priceDetail.orderDiscount = orderDiscount;
            orderTotalDiscount += orderDiscount;
            return orderTotalDiscount;
        }
    }, {
        key: "addOrderChargeInfo2Item",
        value: function addOrderChargeInfo2Item(orderInfo, itemInfo, orderSubtotal, orderTotalCharge) {
            var orderChargeObj = orderInfo.chargeObj;
            var priceDetail = itemInfo.itemPriceDetail;

            var itemTotalPrice = priceDetail.itemSubtotal - priceDetail.itemDiscount + priceDetail.itemCharge;
            var orderCharge = 0;
            for (var i in orderChargeObj) {
                if (!!orderChargeObj[i].chargeIsPer) {
                    orderCharge += itemTotalPrice * orderChargeObj[i].chargeRate;
                } else {
                    if (orderSubtotal > 0) {
                        orderCharge += itemTotalPrice / orderSubtotal * orderChargeObj[i].charge;
                    }
                }
            }

            priceDetail.orderCharge = orderCharge;
            orderTotalCharge += orderCharge;
            return orderTotalCharge;
        }
    }, {
        key: "orderDiscountChargeCalculator",
        value: function orderDiscountChargeCalculator(orderInfo, orderSubtotal) {
            var orderDiscountObj = orderInfo.discountObj;
            var orderChargeObj = orderInfo.chargeObj;
            var orderDiscount = 0;
            var orderCharge = 0;

            // calculate order discount
            for (var i in orderDiscountObj) {
                var discountInfo = orderDiscountObj[i];
                if (!!discountInfo.discountIsPer) {
                    orderDiscount += orderSubtotal * discountInfo.discountRate;
                } else {
                    orderDiscount += discountInfo.discount;
                }
            }

            // calculate order charge
            for (var i in orderChargeObj) {
                var chargeInfo = orderChargeObj[i];
                if (!!chargeInfo.chargeIsPer) {
                    orderCharge += orderSubtotal * chargeInfo.chargeRate;
                } else {
                    orderCharge += chargeInfo.charge;
                }
            }

            return {
                orderDiscount: orderDiscount,
                orderCharge: orderCharge
            };
        }
    }]);

    return DiscountChargeCalculator;
}();