// Karma configuration
// Generated on Tue Aug 28 2018 13:31:47 GMT-0400 (Eastern Daylight Time)

module.exports = function(config) {
  config.set({
    basePath: '',
    frameworks: ['mocha', 'chai'],
    files: [
      'src/*.js',
      'test/*.spec.js'
    ],
    exclude: [
      '**/*.swp'
    ],
    preprocessors: {
      'src/*.js': ['babel', 'coverage'],
      'test/*.spec.js': ['babel']
    },
    reporters: ['mocha', 'coverage'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['PhantomJS'],
    singleRun: false,
    concurrency: Infinity,
    coverageReporter: {
      reporters:[
        {type: 'html', dir: 'coverage/'},
        {type: 'text-summary'}
      ],
    }
  });
}
