"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var SalesTaxCalculator = function () {
    function SalesTaxCalculator() {
        _classCallCheck(this, SalesTaxCalculator);
    }

    _createClass(SalesTaxCalculator, [{
        key: "initObjBasicPriceInfo",
        value: function initObjBasicPriceInfo(thisObj) {
            thisObj.subTotal = 0;
            thisObj.itemDiscount = 0;
            thisObj.itemCharge = 0;
            thisObj.orderDiscount = 0;
            thisObj.orderCharge = 0;
            return thisObj;
        }
    }, {
        key: "accumulateObjBasicPriceInfo",
        value: function accumulateObjBasicPriceInfo(thisObj, priceInfo) {
            thisObj.itemDiscount += priceInfo.itemDiscount;
            thisObj.itemCharge += priceInfo.itemCharge;
            thisObj.orderDiscount += priceInfo.orderDiscount;
            thisObj.orderCharge += priceInfo.orderCharge;
            return thisObj;
        }
    }, {
        key: "itemSalesTaxDetailCalculation",
        value: function itemSalesTaxDetailCalculation(itemInfo, itemTaxDetail, itemTaxObj) {
            var taxId = itemTaxObj.taxId;
            itemTaxDetail[taxId] = {};
            itemTaxDetail[taxId].subTotal = itemInfo.itemPriceDetail.itemSubtotal;
            itemTaxDetail[taxId].itemDiscount = itemInfo.itemPriceDetail.itemDiscount;
            itemTaxDetail[taxId].itemCharge = itemInfo.itemPriceDetail.itemCharge;
            itemTaxDetail[taxId].orderDiscount = itemInfo.itemPriceDetail.orderDiscount;
            itemTaxDetail[taxId].orderCharge = itemInfo.itemPriceDetail.orderCharge;
            itemTaxDetail[taxId].taxRate = itemTaxObj.taxRate;
            return itemTaxDetail;
        }
    }, {
        key: "itemSalesTaxCalculation",
        value: function itemSalesTaxCalculation(itemInfo) {
            var itemTaxDetail = {};
            var itemTaxObj = itemInfo.taxObj;
            for (var i in itemTaxObj) {
                this.itemSalesTaxDetailCalculation(itemInfo, itemTaxDetail, itemTaxObj[i]);
            }
            itemInfo.itemPriceDetail.itemTaxDetail = itemTaxDetail;
        }
    }, {
        key: "orderTaxDetailCalculation",
        value: function orderTaxDetailCalculation(orderTaxDetail, itemTaxDetailInfo) {
            if (orderTaxDetail == undefined) {
                orderTaxDetail = {};
                this.initObjBasicPriceInfo(orderTaxDetail);
                orderTaxDetail.taxRate = itemTaxDetailInfo.taxRate;
            }
            this.accumulateObjBasicPriceInfo(orderTaxDetail, itemTaxDetailInfo);
            orderTaxDetail.subTotal += itemTaxDetailInfo.subTotal;
            return orderTaxDetail;
        }
    }, {
        key: "orderTaxDetailAccumulation",
        value: function orderTaxDetailAccumulation(itemInfo, orderTaxDetail) {
            var itemTaxDetail = itemInfo.itemPriceDetail.itemTaxDetail;
            for (var i in itemTaxDetail) {
                this.orderTaxDetailCalculation(orderTaxDetail[i], itemTaxDetail[i]);
            }
            return orderTaxDetail;
        }
    }, {
        key: "discountCharge4OrderTaxCalculation",
        value: function discountCharge4OrderTaxCalculation(orderTaxDetail, isTaxWithDiscount, isTaxWithCharge) {
            if (!!isTaxWithDiscount) {
                var discount = orderTaxDetail.itemDiscount + orderTaxDetail.orderDiscount;
            } else {
                var discount = 0;
            }
            if (!!isTaxWithCharge) {
                var charge = orderTaxDetail.itemCharge + orderTaxDetail.orderCharge;
            } else {
                var charge = 0;
            }
            return {
                discount: discount,
                charge: charge
            };
        }
    }, {
        key: "orderSalesTaxCalculation",
        value: function orderSalesTaxCalculation(orderInfo, orderTaxDetail) {
            var newOrderTaxDetail = {};
            var isTaxWithDiscount = orderInfo.isTaxWithDiscount;
            var isTaxWithCharge = orderInfo.isTaxWithCharge;

            for (var i in orderTaxDetail) {
                var taxRate = orderTaxDetail[i].taxRate;
                var discountChargeInfo = this.discountCharge4OrderTaxCalculation(orderTaxDetail[i], isTaxWithDiscount, isTaxWithCharge);
                newOrderTaxDetail[i] = {};
                newOrderTaxDetail[i].taxAmount = (orderTaxDetail[i].subTotal - discountChargeInfo.discount + discountChargeInfo.charge) * taxRate;
            }
            return newOrderTaxDetail;
        }
    }]);

    return SalesTaxCalculator;
}();