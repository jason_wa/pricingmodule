'use strict';

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Created by Jason on 8/22/2018.
 */

var DiscountChargeCalculator = function () {
    function DiscountChargeCalculator() {
        _classCallCheck(this, DiscountChargeCalculator);
    }

    _createClass(DiscountChargeCalculator, [{
        key: 'itemDiscountChargeCalculator',
        value: function itemDiscountChargeCalculator(itemInfo) {
            var itemSubtotal = 0;
            var itemCharge = 0;
            var itemDiscount = 0;
            var itemOptionsPrice = 0;
            var isPromotion = itemInfo.isApplicablePromotion;
            var itemPromotion = 0;
            var itemDiscountInfo = itemInfo.discountObj;
            var itemChargeInfo = itemInfo.chargeObj;

            // item subtotal
            for (var i in itemInfo.options) {
                var itemSuboptPrice = 0;
                var optInfo = itemInfo.options[i];
                for (var j in optInfo.subOptions) {
                    var subOptInfo = optInfo.subOptions[j];
                    itemSuboptPrice += subOptInfo.optionPrice * subOptInfo.quantity;
                }
                itemOptionsPrice += (optInfo.optionPrice + itemSuboptPrice) * optInfo.quantity;
            }

            itemSubtotal += (itemInfo.itemPrice + itemOptionsPrice) * itemInfo.quantity;

            // item discount
            for (var i in itemDiscountInfo) {
                var discountInfo = itemDiscountInfo[i];
                if (!!discountInfo.discountIsPer) {
                    itemDiscount += itemSubtotal * discountInfo.discountRate * itemInfo.quantity;
                } else {
                    itemDiscount += discountInfo.discount * itemInfo.quantity;
                }
            }

            // item charge
            for (var i in itemChargeInfo) {
                var chargeInfo = itemChargeInfo[i];
                if (chargeInfo.chargeIsPer) {
                    itemCharge += itemSubtotal * chargeInfo.chargeRate * itemInfo.quantity;
                } else {
                    itemCharge += chargeInfo.charge * itemInfo.quantity;
                }
            }

            // item promotion
            if (!!isPromotion) {
                itemPromotion += itemSubtotal - itemDiscount + itemCharge;
            }

            itemInfo.itemPriceDetail = {
                itemSubtotal: itemSubtotal,
                itemDiscount: itemDiscount,
                itemCharge: itemCharge,
                itemPromotion: itemPromotion
            };
        }
    }, {
        key: 'addOrderDiscountInfo2Item',
        value: function addOrderDiscountInfo2Item(orderInfo, itemInfo, orderSubtotal, orderTotalDiscount) {
            var orderDiscountObj = orderInfo.discountObj;
            var priceDetail = itemInfo.itemPriceDetail;
            var itemTotalPrice = priceDetail.itemSubtotal - priceDetail.itemDiscount + priceDetail.itemCharge;
            var orderDiscount = 0;
            for (var i in orderDiscountObj) {
                if (!!orderDiscountObj[i].discountIsPer) {
                    if (!itemInfo.isSkipDiscount) {
                        orderDiscount += itemTotalPrice * orderDiscountObj[i].discountRate;
                    }
                } else {
                    if (orderSubtotal > 0) {
                        orderDiscount += itemTotalPrice / orderSubtotal * orderDiscountObj[i].discount;
                    }
                }
            }

            priceDetail.orderDiscount = orderDiscount;
            orderTotalDiscount += orderDiscount;
            return orderTotalDiscount;
        }
    }, {
        key: 'addOrderChargeInfo2Item',
        value: function addOrderChargeInfo2Item(orderInfo, itemInfo, orderSubtotal, orderTotalCharge) {
            var orderChargeObj = orderInfo.chargeObj;
            var priceDetail = itemInfo.itemPriceDetail;

            var itemTotalPrice = priceDetail.itemSubtotal - priceDetail.itemDiscount + priceDetail.itemCharge;
            var orderCharge = 0;
            for (var i in orderChargeObj) {
                if (!!orderChargeObj[i].chargeIsPer) {
                    orderCharge += itemTotalPrice * orderChargeObj[i].chargeRate;
                } else {
                    if (orderSubtotal > 0) {
                        orderCharge += itemTotalPrice / orderSubtotal * orderChargeObj[i].charge;
                    }
                }
            }

            priceDetail.orderCharge = orderCharge;
            orderTotalCharge += orderCharge;
            return orderTotalCharge;
        }
    }, {
        key: 'orderDiscountChargeCalculator',
        value: function orderDiscountChargeCalculator(orderInfo, orderSubtotal) {
            var orderDiscountObj = orderInfo.discountObj;
            var orderChargeObj = orderInfo.chargeObj;
            var orderDiscount = 0;
            var orderCharge = 0;

            // calculate order discount
            for (var i in orderDiscountObj) {
                var discountInfo = orderDiscountObj[i];
                if (!!discountInfo.discountIsPer) {
                    orderDiscount += orderSubtotal * discountInfo.discountRate;
                } else {
                    orderDiscount += discountInfo.discount;
                }
            }

            // calculate order charge
            for (var i in orderChargeObj) {
                var chargeInfo = orderChargeObj[i];
                if (!!chargeInfo.chargeIsPer) {
                    orderCharge += orderSubtotal * chargeInfo.chargeRate;
                } else {
                    orderCharge += chargeInfo.charge;
                }
            }

            return {
                orderDiscount: orderDiscount,
                orderCharge: orderCharge
            };
        }
    }]);

    return DiscountChargeCalculator;
}();

var SalesTaxCalculator = function () {
    function SalesTaxCalculator() {
        _classCallCheck(this, SalesTaxCalculator);
    }

    _createClass(SalesTaxCalculator, [{
        key: 'initObjBasicPriceInfo',
        value: function initObjBasicPriceInfo(thisObj) {
            thisObj.subTotal = 0;
            thisObj.itemDiscount = 0;
            thisObj.itemCharge = 0;
            thisObj.orderDiscount = 0;
            thisObj.orderCharge = 0;
            return thisObj;
        }
    }, {
        key: 'accumulateObjBasicPriceInfo',
        value: function accumulateObjBasicPriceInfo(thisObj, priceInfo) {
            thisObj.itemDiscount += priceInfo.itemDiscount;
            thisObj.itemCharge += priceInfo.itemCharge;
            thisObj.orderDiscount += priceInfo.orderDiscount;
            thisObj.orderCharge += priceInfo.orderCharge;
            return thisObj;
        }
    }, {
        key: 'itemSalesTaxDetailCalculation',
        value: function itemSalesTaxDetailCalculation(itemInfo, itemTaxDetail, itemTaxObj) {
            var taxId = itemTaxObj.taxId;
            itemTaxDetail[taxId] = {};
            itemTaxDetail[taxId].subTotal = itemInfo.itemPriceDetail.itemSubtotal;
            itemTaxDetail[taxId].itemDiscount = itemInfo.itemPriceDetail.itemDiscount;
            itemTaxDetail[taxId].itemCharge = itemInfo.itemPriceDetail.itemCharge;
            itemTaxDetail[taxId].orderDiscount = itemInfo.itemPriceDetail.orderDiscount;
            itemTaxDetail[taxId].orderCharge = itemInfo.itemPriceDetail.orderCharge;
            itemTaxDetail[taxId].taxRate = itemTaxObj.taxRate;
            return itemTaxDetail;
        }
    }, {
        key: 'itemSalesTaxCalculation',
        value: function itemSalesTaxCalculation(itemInfo) {
            var itemTaxDetail = {};
            var itemTaxObj = itemInfo.taxObj;
            for (var i in itemTaxObj) {
                this.itemSalesTaxDetailCalculation(itemInfo, itemTaxDetail, itemTaxObj[i]);
            }
            itemInfo.itemPriceDetail.itemTaxDetail = itemTaxDetail;
        }
    }, {
        key: 'orderTaxDetailCalculation',
        value: function orderTaxDetailCalculation(orderTaxDetail, itemTaxDetailInfo) {
            if (orderTaxDetail == undefined) {
                orderTaxDetail = {};
                this.initObjBasicPriceInfo(orderTaxDetail);
                orderTaxDetail.taxRate = itemTaxDetailInfo.taxRate;
            }
            this.accumulateObjBasicPriceInfo(orderTaxDetail, itemTaxDetailInfo);
            orderTaxDetail.subTotal += itemTaxDetailInfo.subTotal;
            return orderTaxDetail;
        }
    }, {
        key: 'orderTaxDetailAccumulation',
        value: function orderTaxDetailAccumulation(itemInfo, orderTaxDetail) {
            var itemTaxDetail = itemInfo.itemPriceDetail.itemTaxDetail;
            for (var i in itemTaxDetail) {
                this.orderTaxDetailCalculation(orderTaxDetail[i], itemTaxDetail[i]);
            }
            return orderTaxDetail;
        }
    }, {
        key: 'discountCharge4OrderTaxCalculation',
        value: function discountCharge4OrderTaxCalculation(orderTaxDetail, isTaxWithDiscount, isTaxWithCharge) {
            if (!!isTaxWithDiscount) {
                var discount = orderTaxDetail.itemDiscount + orderTaxDetail.orderDiscount;
            } else {
                var discount = 0;
            }
            if (!!isTaxWithCharge) {
                var charge = orderTaxDetail.itemCharge + orderTaxDetail.orderCharge;
            } else {
                var charge = 0;
            }
            return {
                discount: discount,
                charge: charge
            };
        }
    }, {
        key: 'orderSalesTaxCalculation',
        value: function orderSalesTaxCalculation(orderInfo, orderTaxDetail) {
            var newOrderTaxDetail = {};
            var isTaxWithDiscount = orderInfo.isTaxWithDiscount;
            var isTaxWithCharge = orderInfo.isTaxWithCharge;

            for (var i in orderTaxDetail) {
                var taxRate = orderTaxDetail[i].taxRate;
                var discountChargeInfo = this.discountCharge4OrderTaxCalculation(orderTaxDetail[i], isTaxWithDiscount, isTaxWithCharge);
                newOrderTaxDetail[i] = {};
                newOrderTaxDetail[i].taxAmount = (orderTaxDetail[i].subTotal - discountChargeInfo.discount + discountChargeInfo.charge) * taxRate;
            }
            return newOrderTaxDetail;
        }
    }]);

    return SalesTaxCalculator;
}();

var CanadaOntarioTaxCalculator = function (_SalesTaxCalculator) {
    _inherits(CanadaOntarioTaxCalculator, _SalesTaxCalculator);

    function CanadaOntarioTaxCalculator() {
        _classCallCheck(this, CanadaOntarioTaxCalculator);

        return _possibleConstructorReturn(this, (CanadaOntarioTaxCalculator.__proto__ || Object.getPrototypeOf(CanadaOntarioTaxCalculator)).call(this));
    }

    _createClass(CanadaOntarioTaxCalculator, [{
        key: 'itemSalesExtraTaxInfoCalculation',
        value: function itemSalesExtraTaxInfoCalculation(itemTaxDetail, itemTaxObj) {
            var taxId = itemTaxObj.taxId;
            itemTaxDetail[taxId].isTaxIncrease = itemTaxObj.extraInfo.isTaxIncrease;
            itemTaxDetail[taxId].priceThreshold = itemTaxObj.extraInfo.priceThreshold;
            itemTaxDetail[taxId].taxIncreaseRate = itemTaxObj.extraInfo.taxIncreaseRate;
        }
    }, {
        key: 'itemSalesTaxCalculation',
        value: function itemSalesTaxCalculation(itemInfo) {
            var itemTaxDetail = {};
            var itemTaxObj = itemInfo.taxObj;
            for (var i in itemTaxObj) {
                itemTaxDetail = _get(CanadaOntarioTaxCalculator.prototype.__proto__ || Object.getPrototypeOf(CanadaOntarioTaxCalculator.prototype), 'itemSalesTaxDetailCalculation', this).call(this, itemInfo, itemTaxDetail, itemTaxObj[i]);
                this.itemSalesExtraTaxInfoCalculation(itemTaxDetail, itemTaxObj[i]);
            }
            itemInfo.itemPriceDetail.itemTaxDetail = itemTaxDetail;
        }
    }, {
        key: 'orderExtraTaxInfoCalculation',
        value: function orderExtraTaxInfoCalculation(orderTaxDetail, itemTaxDetailInfo) {
            orderTaxDetail.isTaxIncrease = itemTaxDetailInfo.isTaxIncrease;
            orderTaxDetail.taxIncreaseRate = itemTaxDetailInfo.taxIncreaseRate;
            orderTaxDetail.priceThreshold = itemTaxDetailInfo.priceThreshold;
        }
    }, {
        key: 'orderTaxDetailAccumulation',
        value: function orderTaxDetailAccumulation(itemInfo, orderTaxDetail) {
            if (orderTaxDetail.taxDetail == undefined) {
                orderTaxDetail.taxDetail = {};
            }
            if (orderTaxDetail.taxListByCategory == undefined) {
                orderTaxDetail.taxListByCategory = {};
            }
            var taxDetail = orderTaxDetail.taxDetail;
            var taxListByCategory = orderTaxDetail.taxListByCategory;

            var itemTaxDetail = itemInfo.itemPriceDetail.itemTaxDetail;
            var taxIdList = [];

            for (var i in itemTaxDetail) {
                taxIdList.push(i);
                taxDetail[i] = _get(CanadaOntarioTaxCalculator.prototype.__proto__ || Object.getPrototypeOf(CanadaOntarioTaxCalculator.prototype), 'orderTaxDetailCalculation', this).call(this, taxDetail[i], itemTaxDetail[i]);
                this.orderExtraTaxInfoCalculation(taxDetail[i], itemTaxDetail[i]);
            }

            if (!!itemInfo.extraInfo.isQuantityRule) {
                var cid = itemInfo.extraInfo.categoryId;
                if (taxListByCategory[cid] == undefined) {
                    taxListByCategory[cid] = {};
                    taxListByCategory[cid] = _get(CanadaOntarioTaxCalculator.prototype.__proto__ || Object.getPrototypeOf(CanadaOntarioTaxCalculator.prototype), 'initObjBasicPriceInfo', this).call(this, taxListByCategory[cid]);
                    taxListByCategory[cid].quantity = 0;
                    taxListByCategory[cid].taxId = taxIdList;
                    taxListByCategory[cid].quantityThreshold = itemInfo.extraInfo.quantityThreshold;
                }
                taxListByCategory[cid] = _get(CanadaOntarioTaxCalculator.prototype.__proto__ || Object.getPrototypeOf(CanadaOntarioTaxCalculator.prototype), 'accumulateObjBasicPriceInfo', this).call(this, taxListByCategory[cid], itemInfo.itemPriceDetail);
                taxListByCategory[cid].subTotal += itemInfo.itemPriceDetail.itemSubtotal;
                taxListByCategory[cid].quantity += itemInfo.quantity;
            }

            return orderTaxDetail;
        }
    }, {
        key: 'orderSalesTaxCalculation',
        value: function orderSalesTaxCalculation(orderInfo, orderTaxDetail) {
            var newOrderTaxDetail = {};
            var taxDetail = orderTaxDetail.taxDetail;
            var taxListByCategory = orderTaxDetail.taxListByCategory;
            var isTaxWithDiscount = orderInfo.isTaxWithDiscount;
            var isTaxWithCharge = orderInfo.isTaxWithCharge;

            for (var i in taxListByCategory) {
                if (taxListByCategory[i].quantity > taxListByCategory[i].quantityThreshold) {
                    var taxIdList = taxListByCategory[i].taxId;
                    for (var j in taxIdList) {
                        taxDetail[taxIdList[j]].subTotal -= taxListByCategory[i].subTotal;
                        taxDetail[taxIdList[j]].itemDiscount -= taxListByCategory[i].itemDiscount;
                        taxDetail[taxIdList[j]].itemCharge -= taxListByCategory[i].itemCharge;
                        taxDetail[taxIdList[j]].orderDiscount -= taxListByCategory[i].orderDiscount;
                        taxDetail[taxIdList[j]].orderCharge -= taxListByCategory[i].orderCharge;
                    }
                }
            }

            for (var i in taxDetail) {
                var taxRate = taxDetail[i].taxRate;
                var discountChargeInfo = _get(CanadaOntarioTaxCalculator.prototype.__proto__ || Object.getPrototypeOf(CanadaOntarioTaxCalculator.prototype), 'discountCharge4OrderTaxCalculation', this).call(this, taxDetail[i], isTaxWithDiscount, isTaxWithCharge);
                var total = orderTaxDetail[i].subTotal - discountChargeInfo.discount + discountChargeInfo.charge;
                if (orderTaxDetail[i].isTaxIncrease && total > orderTaxDetail[i].priceThreshold) {
                    taxRate += orderTaxDetail[i].taxIncreaseRate;
                }
                newOrderTaxDetail[i] = {};
                newOrderTaxDetail[i].taxAmount = total * taxRate;
            }
            return newOrderTaxDetail;
        }
    }]);

    return CanadaOntarioTaxCalculator;
}(SalesTaxCalculator);

var OrderPricing = function () {
    function OrderPricing(discountChargeCalculator, taxCalculator) {
        _classCallCheck(this, OrderPricing);

        this.discountChargeCalculator = discountChargeCalculator;
        this.taxCalculator = taxCalculator;
    }

    _createClass(OrderPricing, [{
        key: 'floatNumberRounding',
        value: function floatNumberRounding(num, precision) {
            var tempNum = parseFloat(num).toFixed(precision + 4);
            return Number(Math.round(tempNum + 'e' + precision) + 'e-' + precision);
        }
    }, {
        key: 'orderDetailPrice',
        value: function orderDetailPrice(orderInfo) {
            var itemInfoList = this.getItemInfoListFromOrderInfo(orderInfo);
            var orderSubtotalPromotion = this.getOrderSubtotalPromotionDetail(itemInfoList);
            var orderTaxDiscountChargeDetail = this.getOrderTaxDiscountChargeDetail(orderInfo, itemInfoList, orderSubtotalPromotion.orderSubtotal);
            var orderTotalTips = this.floatNumberRounding(orderInfo.totalTips, 2);
            var orderTaxDetail = orderTaxDiscountChargeDetail.orderTaxDetail;
            var orderTaxTotal = 0;

            for (var i in orderTaxDetail) {
                orderTaxDetail.taxAmount = this.floatNumberRounding(orderTaxDetail[i].taxAmount, 2);
                orderTaxTotal += orderTaxDetail.taxAmount;
            }
            orderTaxTotal = this.floatNumberRounding(orderTaxTotal, 2);

            var orderOriginalTotal = orderSubtotalPromotion.orderSubtotal + orderTotalTips + orderTaxTotal - orderTaxDiscountChargeDetail.orderDiscount + orderTaxDiscountChargeDetail.orderCharge;
            orderOriginalTotal = this.floatNumberRounding(orderOriginalTotal, 2);

            // rounding strategy
            if (orderInfo.roundingStrategy == "NEAREST_5_CENTS") {
                var rounding = this.floatNumberRounding(orderOriginalTotal * 10000 % 500 / 10000, 2);
            } else if (orderInfo.roundingStrategy == "NEAREST_10_CENTS") {
                var rounding = this.floatNumberRounding(orderOriginalTotal * 10000 % 1000 / 10000, 2);
            } else {
                var rounding = 0;
            }
            var orderTotal = this.floatNumberRounding(orderOriginalTotal - rounding, 2);

            return {
                orderSubtotal: orderSubtotalPromotion.orderSubtotal,
                orderPromotion: orderSubtotalPromotion.orderPromotion,
                orderDiscount: orderTaxDiscountChargeDetail.orderDiscount,
                orderCharge: orderTaxDiscountChargeDetail.orderCharge,
                orderTotalTips: orderTotalTips,
                orderTaxDetail: orderTaxDetail,
                orderTaxTotal: orderTaxTotal,
                orderOriginalTotal: orderOriginalTotal,
                rounding: rounding,
                orderTotal: orderTotal
            };
        }
    }, {
        key: 'getItemInfoListFromOrderInfo',
        value: function getItemInfoListFromOrderInfo(orderInfo) {
            var itemInfoList = [];
            for (var i in orderInfo.itemInfoList) {
                itemInfoList.push(orderInfo.itemInfoList[i]);
            }

            if (orderInfo.parentOrder != undefined && orderInfo.parentOrder.itemInfoList != undefined) {
                var suborderNum = orderInfo.parentOrder.suborderNum;
                for (var j in orderInfo.parentOrder.itemInfoList) {
                    var thisItemInfo = orderInfo.parentOrder.itemInfoList[j];
                    thisItemInfo.quantity = this.floatNumberRounding(thisItemInfo.quantity / suborderNum, 4);
                    itemInfoList.push(thisItemInfo);
                }
            }
            return itemInfoList;
        }
    }, {
        key: 'getOrderSubtotalPromotionDetail',
        value: function getOrderSubtotalPromotionDetail(itemInfoList) {
            var orderSubtotal = 0;
            var orderPromotion = 0;
            for (var i in itemInfoList) {
                var thisItemInfo = itemInfoList[i];
                this.discountChargeCalculator.itemDiscountChargeCalculator(thisItemInfo);
                var itemPriceDetail = thisItemInfo.itemPriceDetail;
                orderSubtotal += itemPriceDetail.itemSubtotal + itemPriceDetail.itemCharge - itemPriceDetail.itemDiscount;
                orderPromotion += itemPriceDetail.itemPromotion;
            }
            return {
                orderSubtotal: this.floatNumberRounding(orderSubtotal, 2),
                orderPromotion: this.floatNumberRounding(orderPromotion, 2)
            };
        }
    }, {
        key: 'getOrderTaxDiscountChargeDetail',
        value: function getOrderTaxDiscountChargeDetail(orderInfo, itemInfoList, orderSubtotal) {
            var orderTaxDetail = {};
            var orderDiscount = 0;
            var orderCharge = 0;
            if (!!orderInfo.isTaxExempt) {
                for (var i in itemInfoList) {
                    orderDiscount = this.discountChargeCalculator.addOrderDiscountInfo2Item(orderInfo, itemInfoList[i], orderSubtotal, orderDiscount);
                    orderCharge = this.discountChargeCalculator.addOrderChargeInfo2Item(orderInfo, itemInfoList[i], orderSubtotal, orderCharge);
                }
            } else {
                for (var i in itemInfoList) {
                    orderDiscount = this.discountChargeCalculator.addOrderDiscountInfo2Item(orderInfo, itemInfoList[i], orderSubtotal, orderDiscount);
                    orderCharge = this.discountChargeCalculator.addOrderChargeInfo2Item(orderInfo, itemInfoList[i], orderSubtotal, orderCharge);
                    this.taxCalculator.itemSalesTaxCalculation(itemInfoList[i]);
                    orderTaxDetail = this.taxCalculator.orderTaxDetailAccumulation(itemInfoList[i], orderTaxDetail);
                }
                orderTaxDetail = this.taxCalculator.orderSalesTaxCalculation(orderInfo, orderTaxDetail);
            }
            return {
                orderDiscount: this.floatNumberRounding(orderDiscount, 2),
                orderCharge: this.floatNumberRounding(orderCharge, 2),
                orderTaxDetail: orderTaxDetail
            };
        }
    }]);

    return OrderPricing;
}();