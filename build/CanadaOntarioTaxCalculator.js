"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var CanadaOntarioTaxCalculator = function (_SalesTaxCalculator) {
    _inherits(CanadaOntarioTaxCalculator, _SalesTaxCalculator);

    function CanadaOntarioTaxCalculator() {
        _classCallCheck(this, CanadaOntarioTaxCalculator);

        return _possibleConstructorReturn(this, (CanadaOntarioTaxCalculator.__proto__ || Object.getPrototypeOf(CanadaOntarioTaxCalculator)).call(this));
    }

    _createClass(CanadaOntarioTaxCalculator, [{
        key: "itemSalesExtraTaxInfoCalculation",
        value: function itemSalesExtraTaxInfoCalculation(itemTaxDetail, itemTaxObj) {
            var taxId = itemTaxObj.taxId;
            itemTaxDetail[taxId].isTaxIncrease = itemTaxObj.extraInfo.isTaxIncrease;
            itemTaxDetail[taxId].priceThreshold = itemTaxObj.extraInfo.priceThreshold;
            itemTaxDetail[taxId].taxIncreaseRate = itemTaxObj.extraInfo.taxIncreaseRate;
        }
    }, {
        key: "itemSalesTaxCalculation",
        value: function itemSalesTaxCalculation(itemInfo) {
            var itemTaxDetail = {};
            var itemTaxObj = itemInfo.taxObj;
            for (var i in itemTaxObj) {
                itemTaxDetail = _get(CanadaOntarioTaxCalculator.prototype.__proto__ || Object.getPrototypeOf(CanadaOntarioTaxCalculator.prototype), "itemSalesTaxDetailCalculation", this).call(this, itemInfo, itemTaxDetail, itemTaxObj[i]);
                this.itemSalesExtraTaxInfoCalculation(itemTaxDetail, itemTaxObj[i]);
            }
            itemInfo.itemPriceDetail.itemTaxDetail = itemTaxDetail;
        }
    }, {
        key: "orderExtraTaxInfoCalculation",
        value: function orderExtraTaxInfoCalculation(orderTaxDetail, itemTaxDetailInfo) {
            orderTaxDetail.isTaxIncrease = itemTaxDetailInfo.isTaxIncrease;
            orderTaxDetail.taxIncreaseRate = itemTaxDetailInfo.taxIncreaseRate;
            orderTaxDetail.priceThreshold = itemTaxDetailInfo.priceThreshold;
        }
    }, {
        key: "orderTaxDetailAccumulation",
        value: function orderTaxDetailAccumulation(itemInfo, orderTaxDetail) {
            if (orderTaxDetail.taxDetail == undefined) {
                orderTaxDetail.taxDetail = {};
            }
            if (orderTaxDetail.taxListByCategory == undefined) {
                orderTaxDetail.taxListByCategory = {};
            }
            var taxDetail = orderTaxDetail.taxDetail;
            var taxListByCategory = orderTaxDetail.taxListByCategory;

            var itemTaxDetail = itemInfo.itemPriceDetail.itemTaxDetail;
            var taxIdList = [];

            for (var i in itemTaxDetail) {
                taxIdList.push(i);
                taxDetail[i] = _get(CanadaOntarioTaxCalculator.prototype.__proto__ || Object.getPrototypeOf(CanadaOntarioTaxCalculator.prototype), "orderTaxDetailCalculation", this).call(this, taxDetail[i], itemTaxDetail[i]);
                this.orderExtraTaxInfoCalculation(taxDetail[i], itemTaxDetail[i]);
            }

            if (!!itemInfo.extraInfo.isQuantityRule) {
                var cid = itemInfo.extraInfo.categoryId;
                if (taxListByCategory[cid] == undefined) {
                    taxListByCategory[cid] = {};
                    taxListByCategory[cid] = _get(CanadaOntarioTaxCalculator.prototype.__proto__ || Object.getPrototypeOf(CanadaOntarioTaxCalculator.prototype), "initObjBasicPriceInfo", this).call(this, taxListByCategory[cid]);
                    taxListByCategory[cid].quantity = 0;
                    taxListByCategory[cid].taxId = taxIdList;
                    taxListByCategory[cid].quantityThreshold = itemInfo.extraInfo.quantityThreshold;
                }
                taxListByCategory[cid] = _get(CanadaOntarioTaxCalculator.prototype.__proto__ || Object.getPrototypeOf(CanadaOntarioTaxCalculator.prototype), "accumulateObjBasicPriceInfo", this).call(this, taxListByCategory[cid], itemInfo.itemPriceDetail);
                taxListByCategory[cid].subTotal += itemInfo.itemPriceDetail.itemSubtotal;
                taxListByCategory[cid].quantity += itemInfo.quantity;
            }

            return orderTaxDetail;
        }
    }, {
        key: "orderSalesTaxCalculation",
        value: function orderSalesTaxCalculation(orderInfo, orderTaxDetail) {
            var newOrderTaxDetail = {};
            var taxDetail = orderTaxDetail.taxDetail;
            var taxListByCategory = orderTaxDetail.taxListByCategory;
            var isTaxWithDiscount = orderInfo.isTaxWithDiscount;
            var isTaxWithCharge = orderInfo.isTaxWithCharge;

            for (var i in taxListByCategory) {
                if (taxListByCategory[i].quantity > taxListByCategory[i].quantityThreshold) {
                    var taxIdList = taxListByCategory[i].taxId;
                    for (var j in taxIdList) {
                        taxDetail[taxIdList[j]].subTotal -= taxListByCategory[i].subTotal;
                        taxDetail[taxIdList[j]].itemDiscount -= taxListByCategory[i].itemDiscount;
                        taxDetail[taxIdList[j]].itemCharge -= taxListByCategory[i].itemCharge;
                        taxDetail[taxIdList[j]].orderDiscount -= taxListByCategory[i].orderDiscount;
                        taxDetail[taxIdList[j]].orderCharge -= taxListByCategory[i].orderCharge;
                    }
                }
            }

            for (var i in taxDetail) {
                var taxRate = taxDetail[i].taxRate;
                var discountChargeInfo = _get(CanadaOntarioTaxCalculator.prototype.__proto__ || Object.getPrototypeOf(CanadaOntarioTaxCalculator.prototype), "discountCharge4OrderTaxCalculation", this).call(this, taxDetail[i], isTaxWithDiscount, isTaxWithCharge);
                var total = orderTaxDetail[i].subTotal - discountChargeInfo.discount + discountChargeInfo.charge;
                if (orderTaxDetail[i].isTaxIncrease && total > orderTaxDetail[i].priceThreshold) {
                    taxRate += orderTaxDetail[i].taxIncreaseRate;
                }
                newOrderTaxDetail[i] = {};
                newOrderTaxDetail[i].taxAmount = total * taxRate;
            }
            return newOrderTaxDetail;
        }
    }]);

    return CanadaOntarioTaxCalculator;
}(SalesTaxCalculator);