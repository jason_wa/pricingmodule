
export default class SalesTaxCalculator{

    initObjBasicPriceInfo(thisObj){
        thisObj.subTotal = 0;
        thisObj.itemDiscount = 0;
        thisObj.itemCharge = 0;
        thisObj.orderDiscount = 0;
        thisObj.orderCharge = 0;
        return thisObj;
    }

    accumulateObjBasicPriceInfo(thisObj,priceInfo){
        thisObj.itemDiscount += priceInfo.itemDiscount;
        thisObj.itemCharge += priceInfo.itemCharge;
        thisObj.orderDiscount += priceInfo.orderDiscount;
        thisObj.orderCharge += priceInfo.orderCharge;
        return thisObj;
    }

    itemSalesTaxDetailCalculation(itemInfo,itemTaxDetail,itemTaxObj){
        var taxId = itemTaxObj.taxId;
        itemTaxDetail[taxId] = {};
        itemTaxDetail[taxId].subTotal = itemInfo.itemPriceDetail.itemSubtotal;
        itemTaxDetail[taxId].itemDiscount = itemInfo.itemPriceDetail.itemDiscount;
        itemTaxDetail[taxId].itemCharge = itemInfo.itemPriceDetail.itemCharge;
        itemTaxDetail[taxId].orderDiscount = itemInfo.itemPriceDetail.orderDiscount;
        itemTaxDetail[taxId].orderCharge = itemInfo.itemPriceDetail.orderCharge;
        itemTaxDetail[taxId].taxRate = itemTaxObj.taxRate;
        itemTaxDetail[taxId].taxName = itemTaxObj.taxName;
        return itemTaxDetail;
    }

    itemSalesTaxCalculation(itemInfo){
        var itemTaxDetail = {};
        var itemTaxObj = itemInfo.taxObj;
        for(var i in itemTaxObj){
            this.itemSalesTaxDetailCalculation(itemInfo,itemTaxDetail,itemTaxObj[i]);
        }
        itemInfo.itemPriceDetail.itemTaxDetail = itemTaxDetail;
    }

    orderTaxDetailCalculation(orderTaxDetail,itemTaxDetailInfo){
        if(orderTaxDetail == undefined){
            orderTaxDetail = {};
            orderTaxDetail = this.initObjBasicPriceInfo(orderTaxDetail);
            orderTaxDetail.taxRate = itemTaxDetailInfo.taxRate;
            orderTaxDetail.taxName = itemTaxDetailInfo.taxName;
        }
        this.accumulateObjBasicPriceInfo(orderTaxDetail,itemTaxDetailInfo);
        orderTaxDetail.subTotal += itemTaxDetailInfo.subTotal;
        return orderTaxDetail;
    }

    orderTaxDetailAccumulation(itemInfo,orderTaxDetail){
        var itemTaxDetail = itemInfo.itemPriceDetail.itemTaxDetail;
        for(var i in itemTaxDetail){
            orderTaxDetail[i] = this.orderTaxDetailCalculation(orderTaxDetail[i],itemTaxDetail[i]);
        }
        return orderTaxDetail;
    }

    discountCharge4OrderTaxCalculation(orderTaxDetail,isTaxWithDiscount,isTaxWithCharge){
        if(!!isTaxWithDiscount){
            var discount = orderTaxDetail.itemDiscount + orderTaxDetail.orderDiscount;
        }
        else{
            var discount = 0;
        }
        if(!!isTaxWithCharge){
            var charge = orderTaxDetail.itemCharge + orderTaxDetail.orderCharge;
        }
        else{
            var charge = 0;
        }
        return {
            discount: discount,
            charge: charge
        }
    }

    orderSalesTaxCalculation(orderInfo, orderTaxDetail){
        var newOrderTaxDetail = {};
        var isTaxWithDiscount = orderInfo.isTaxWithDiscount;
        var isTaxWithCharge = orderInfo.isTaxWithCharge;

        for(var i in orderTaxDetail){
            var taxRate = orderTaxDetail[i].taxRate;
            var taxName = orderTaxDetail[i].taxName;
            var discountChargeInfo = this.discountCharge4OrderTaxCalculation(orderTaxDetail[i],isTaxWithDiscount,isTaxWithCharge);
            newOrderTaxDetail[i] = {};
            newOrderTaxDetail[i].taxName = taxName;
            newOrderTaxDetail[i].taxRate = taxRate;
            newOrderTaxDetail[i].taxAmount = (orderTaxDetail[i].subTotal - discountChargeInfo.discount + discountChargeInfo.charge) * taxRate;
        }
        return newOrderTaxDetail;
    }
}